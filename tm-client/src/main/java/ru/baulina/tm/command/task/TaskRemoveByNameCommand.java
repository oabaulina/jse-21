package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.endpoint.Task;
import ru.baulina.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Session session = getSession();
        @Nullable final Task task = endpointLocator.getTaskEndpoint().removeOneTaskByName(session, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        System.out.println();
    }

}
