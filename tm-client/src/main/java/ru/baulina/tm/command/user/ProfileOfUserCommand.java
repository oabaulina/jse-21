package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.endpoint.User;
import ru.baulina.tm.util.TerminalUtil;

public final class ProfileOfUserCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "profiler-of-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user's profiler.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW_PROFILE_OF_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        @Nullable final Session session = getSession();
        final User user = endpointLocator.getAdminUserEndpoint().findUserByLogin(session, login);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FEST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
        System.out.println();
    }

}
