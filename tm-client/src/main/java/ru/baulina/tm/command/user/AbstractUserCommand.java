package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.Session;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.user.AccessDeniedException;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

    public Session getSession() {
        @NotNull final Bootstrap bootstrap = (Bootstrap) endpointLocator;
        @Nullable final Session session = bootstrap.getSession();
        if (session == null) throw new AccessDeniedException();
        return session;
    }

}
