package ru.baulina.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.baulina.tm.api.endpoint.IEndpointLocator;
import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.*;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.exception.system.UnknownCommandException;
import ru.baulina.tm.repository.CommandRepository;
import ru.baulina.tm.service.CommandService;
import ru.baulina.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

public final class Bootstrap implements IEndpointLocator, IServiceLocator {

    @NotNull
    private final AdminDumpEndpointService adminDampEndpointService = new AdminDumpEndpointService();

    @NotNull
    private final AdminDumpEndpoint adminDampEndpoint = adminDampEndpointService.getAdminDumpEndpointPort();

    @NotNull
    private final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Nullable
    private Session session = null;

    public void setSession(@Nullable final Session session) {
        this.session = session;
    }

    @Nullable
    public Session getSession() {
       return session;
    }

    @Nullable
    public Long getUserId() {
        if (session == null) return null;
        return session.getUserId();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.baulina.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.baulina.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        command.setEndpointLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) throws Exception {
        initCommands();
        System.out.println("** Welcome to task manager **");
        System.out.println();
        try {
            if (parseArgs(args)) System.exit(0);
        } catch (Exception e) {
            logError(e);
            System.exit(0);
        }
        process();
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        @Nullable final AbstractCommand command = commandService.getByArg(arg);
        if (command == null) throw new UnknownCommandException(arg);
        command.execute();
        return true;
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    @Override
    public @NotNull AdminDumpEndpoint getAdminDampEndpoint() {
        return adminDampEndpoint;
    }

    @Override
    public @NotNull AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @Override
    public @NotNull ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    public @NotNull SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    public @NotNull TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    public @NotNull UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    public @NotNull ICommandService getCommandService() {
        return commandService;
    }

}
