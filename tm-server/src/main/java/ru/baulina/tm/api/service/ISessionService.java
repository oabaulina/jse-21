package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.List;

public interface ISessionService {

    @Nullable
    Session open(@Nullable final String login, @Nullable final String password);

    void close(@Nullable final Session session);

    void closeAll(@Nullable final Session session);

    boolean isValid(@Nullable final Session session);

    void validate(@Nullable final Session session);

    void validate(@Nullable final Session session, @Nullable final Role role);

    boolean isTimeOut(@NotNull Long timestamp);

    @Nullable
    User getUser(@Nullable final Session session);

    @Nullable
    Long getUserId(@Nullable final Session session);

    @NotNull
    List<Session> getListSession(@Nullable final Session session);

    @Nullable
    String sign(@Nullable final Session session);

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password);

    void signOutByLogin(@Nullable final String login);

    void SignOutByUserId(@Nullable final Long userId);

}
