package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable final Domain domain);

    void export(@Nullable final Domain domain);

}
