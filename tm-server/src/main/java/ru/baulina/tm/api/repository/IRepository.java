package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> getList();

    void merge(@Nullable final List<E> entities);

    void merge(@Nullable final E... entities);

    void merge(@Nullable final E entity);

    void load(@NotNull final List<E> entities);

    void load(@NotNull final E... entities);

    void clear();

}
