package ru.baulina.tm.endpoint;

import ru.baulina.tm.api.endpoint.IAdminUserEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IServiceLocator serviceLocator;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public List<User> getUserList(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().getList();
    }

    @Override
    @WebMethod
    public User findUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") final Long id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findById(id);
    }

    @Override
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @WebMethod
    public User removeUserById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") final Long id
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().removeById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void lockUserLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserLogin(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserLogin(login);
    }

}
