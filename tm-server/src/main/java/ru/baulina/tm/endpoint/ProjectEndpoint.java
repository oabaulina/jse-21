package ru.baulina.tm.endpoint;

import ru.baulina.tm.api.endpoint.IProjectEndpoint;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public List<Project> getProjectList() {
        return serviceLocator.getProjectService().getList();
    }

    @Override
    @WebMethod
    public void loadProjects(
            @WebParam(name = "projects", partName = "projects") List<Project> projects
    ) {
        serviceLocator.getProjectService().load(projects);
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getProjectService().create(userId, name);
    }

    @Override
    @WebMethod
    public void createProjectWithDescription(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getProjectService().create(userId, name, description);
    }

    @Override
    @WebMethod
    public void addProject(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "project", partName = "project") Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getProjectService().add(userId, project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "project", partName = "project") Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getProjectService().remove(userId, project);
    }

    @Override
    @WebMethod
    public List<Project> findAllProjects(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().findAll(userId);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getProjectService().clear(userId);
    }

    @Override
    @WebMethod
    public Project findOneProjectById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().findOneById(userId, id);
    }

    @Override
    @WebMethod
    public Project findOneProjectByIndex(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().findOneByIndex(userId, index);
    }

    @Override
    @WebMethod
    public Project findOneProjectByName(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().findOneByName(userId, name);
    }

    @Override
    @WebMethod
    public Project removeOneProjectById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().removeOneById(userId, id);
    }

    @Override
    @WebMethod
    public Project removeOneProjectByIndex(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().removeOneByIndex(userId, index);
    }

    @Override
    @WebMethod
    public Project removeOneProjectByName(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().removeOneByName(userId, name);
    }

    @Override
    @WebMethod
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().updateProjectById(userId, id, name, description);
    }

    @Override
    @WebMethod
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Long userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectService().updateProjectByIndex(userId, index, name, description);
    }

}
