package ru.baulina.tm.exception.empty;

public class EmptyPasswordException extends RuntimeException{

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
