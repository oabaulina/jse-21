package ru.baulina.tm.exception.session;

public class SessionTimeOutException extends RuntimeException {

    public SessionTimeOutException() {
        super("Error! Time out...");
    }

}