package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ISessionRepository;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.api.service.IServiceLocator;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.empty.EmptyLoginException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.session.SessionTimeOutException;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.util.HashUtil;
import ru.baulina.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    private final ISessionRepository repository;

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    public SessionService(
            @NotNull final ISessionRepository repository,
            @NotNull final IServiceLocator serviceLocator) {
        this.repository = repository;
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        repository.merge(session);
        final String signature = sign(session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable final Session session) {
        validate(session);
        repository.remove(session);
    }

    @Override
    public void closeAll(@Nullable final Session session) {
        validate(session);
        if (session.getUserId() == null) throw new AccessDeniedException();
        repository.removeByUserId(session.getUserId());
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId() < 0) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        if (isTimeOut(session.getTimestamp())) {
            close(session);
            throw new SessionTimeOutException();
        }
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        temp.setSignature(null);
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp);
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!repository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final Long userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals((user.getRole()))) throw new AccessDeniedException();
    }

    @Override
    public boolean isTimeOut(@NotNull final Long timeStamp) {
        @NotNull Long currentTime = System.currentTimeMillis();
        int timeOut = 60 * 60 * 1000;
        return ((currentTime - timeStamp) > timeOut);
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) {
        final Long userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @Nullable
    @Override
    public Long getUserId(@Nullable final Session session) {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getListSession(@Nullable final Session session) {
        validate(session);
        return repository.getList();
    }

    @Nullable
    @Override
    public String sign(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String salt = propertyService.getSessionSalt();
        @Nullable final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        return signature;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new AccessDeniedException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();;
        @NotNull final Long userId = user.getId();
        repository.removeByUserId(userId);
    }

    @Override
    public void SignOutByUserId(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new AccessDeniedException();
        repository.removeByUserId(userId);
    }

}
