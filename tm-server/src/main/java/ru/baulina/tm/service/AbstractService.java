package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.api.service.IService;
import ru.baulina.tm.entity.AbstractEntity;

import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    @NotNull
    @Override
    public List<E> getList() {
        return repository.getList();
    }

    @Override
    public void load(@Nullable List<E> entities) {
        repository.clear();
        if (entities == null) {return;}
        repository.load(entities);
    }

}
